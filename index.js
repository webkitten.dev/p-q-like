class P {
  constructor() {
    this.results = []
    this.errors = []
  }

  defer() {
    let resolved
    let rejected
    const p = new Promise((resolve, reject) => {
      resolved = resolve
      rejected = reject
    })
    return { promise: p, resolve: resolved, reject: rejected }
  }

  delay(timeout) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve()
      }, timeout)
    })
  }

  forEach(array, iterator, concurrency = 1) {
    return new Promise(resolve => {
      const results = []
      const errors = []
      const finished = []
      if (!array || !array.length) resolve({results, errors})
      if (concurrency === 0 || concurrency < 0) concurrency = array.length

      Array(concurrency).fill(array.entries()).map(async (cursor) => {
        for (const [index, value] of cursor){
          await iterator(value, index).then(result => {
            results.push(result)
          }).catch(e => {
            errors.push(e)
          }).finally(() => {
            finished.push(index)
            if (finished.length === array.length) {
              resolve({results, errors})
            }
          })
        }
      })
    })
  }
}

module.exports = new P()
module.exports.default = new P()
