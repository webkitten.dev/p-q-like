# p-q-like

[Q](https://www.npmjs.com/package/q)-like module for performing the most commonly used asynchronous operations with zero-dependency.

## Installation

using npm:

```sh
npm install p-q-like
```

using yarn:

```sh
yarn add p-q-like
```

## Usage

```js
// CommonJS `require`
const P = require('p-q-like')

// ECMAScript `import`
import P from 'p-q-like'
```

### Deferred promise

```js
// Promised function without nesting
function someAsyncFunctionExample1() {
  const deferred = P.defer()
  setTimeout(() => {
    deferred.resolve()
  }, 1000)
  return deferred.promise
}
```

```js
// One more example
const someAsyncFunctionExample2 = async () => {
  const { promise, resolve } = P.defer()
  await P.delay(1000)
  resolve()
  return promise
}
```

### Delay in the synchronous execution style

```js
// Thenable delay in milliseconds
P.delay(1000).then(() => {
  console.log('Delayed function executed after 1 second!')
})

// IIFE async await
(async () => {
  await P.delay(1000)
})()
```


### Async operation on each item in an array

```js
const array = [1, 2, 3]

// Supports concurrency modes
const concurrency = 1 // || 0: in parallel || 2+: in parallel with concurrency

const iterateEachOther = (value, index) => {
  const deferred = P.defer()
  setTimeout(() => {
    console.log(`Item at index ${index}: ${value}`)
    deferred.resolve()
  }, 1000)
  return deferred.promise
}

(async () => {
  const { results, errors } = await P.forEach(array, iterateEachOther, concurrency)

  if (!errors.length) console.log('All operations completed!')
})()
```

## API Reference

### defer()

Returns a deferred promise that can be resolved or rejected later.

```js
const someAsync = async () => {
  const deferred = P.defer()
  await someOtherAsync()
  deferred.resolve()
  return deferred.promise
}
```

### delay(timeout)

Returns a promise that resolves after a specified amount of time.

```js
// pause for 1 second
await P.delay(1000)
```

### forEach(array, iterator, concurrency = 1)

Performs an async operation on each item in an array, with optional concurrency mode.

Returns a promise that resolves with the results and errors of the operations.

```js
P.forEach(['zero', 'one', 'two'], (value, index) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(`Item at index ${index}: ${value}`)
      resolve()
    }, 1000)
  })
}).then(({ results, errors }) => {
  console.log('All operations completed!')
})

// => Result
// Item at index 0: zero
// ...one second later...
// Item at index 1: one
// ...one second later...
// Item at index 2: two
// ...one second later...
// All operations completed!
```

## Concurrency modes

### 0

execute in parallel (Promise.all)

### 1

(default) execute one by one

### 2

execute in parallel with concurrency mode

## License

MIT